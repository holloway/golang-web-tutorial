package module

var (
	provider Provider
)

// Provider is an interface for drivers to construct and build against it
type Provider interface {
	Speak() string
}

// Register is for driver packages to register the interface provider
func Register(p Provider) error {
	provider = p
	return nil
}

// Narrates is the interface function that prints the output
func Narrates() string {
	if provider != nil {
		return provider.Speak()
	}
	return ""
}
