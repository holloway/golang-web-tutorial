package moduleA

import (
	"gitlab.com/holloway/golang-web-tutorial/drivers_packages/module"
)

type moduleA struct {
	message string
}

func (m moduleA) Speak() string {
	return m.message
}

func init() {
	module.Register( moduleA{
		message: "chorons",
	})
}
