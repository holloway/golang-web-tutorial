package main

import (
	"fmt"
	"gitlab.com/holloway/golang-web-tutorial/drivers_packages/module"
	_ "gitlab.com/holloway/golang-web-tutorial/drivers_packages/module/drivers/b"
)

func main() {
	fmt.Println(module.Narrates())
}
