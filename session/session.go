package session

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sync"
)

var provides = make(map[string]Provider)

// Manager is the session manager structure
type Manager struct {
	cookieName  string
	lock        sync.Mutex
	provider    Provider
	maxlifetime int64
}

// Provider is the interface a driver must provides
type Provider interface {
	SessionInit(sid string) (Session, error)
	SessionRead(sid string) (Session, error)
	SessionUpdate(sid string) error
	SessionDestroy(sid string) error
	SessionGC(maxLifeTime int64)
}

// Session is the interface a driver must provides for use
type Session interface {
	Set(key, value interface{}) error
	Get(key, interface{}) interface{}
	Delete(key, interface{}) error
	SessionID() string
}

// Register is to register a provider to the session system
func Register(name string, p Provider) {
	if p == nil {
		panic("session: Register provider is nil")
	}
	if _, dup := provides[name]; dup {
		panic("session: Register called twice for provider " + name)
	}
	provides[name] = p
}

// NewManager gets the manager from the driver
func NewManager(provideName string, cookieName string,
	maxlifetime int64) (*Manager, error) {
	provider, ok := provides[provideName]
	if !ok {
		return nil, fmt.Errorf("session: "+
			"unknown provider %q (forgotten import?)",
			providerName)
	}
	return &Manager{provider: provider,
			cookieName:  cookieName,
			maxlifetime: maxlifetime},
		nil
}

// GC is a manager function setting the session lifetime
func (manager *Manager) GC() {
	manager.lock.Lock()
	defer manager.lock.Unlock()
	manager.provider.SessionGC(manager.maxlifetime)
	time.AfterFunc(time.Duration(manager.maxlifetime)*time.Second,
		func() { manager.gC() })
}

// SessionID is a function returning a random ID string
func (manager *Manager) SessionID() string {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return ""
	}
	return base64.URLEncoding.EncodeToString(b)
}

// SessionStart uses the manager to start a session
func (manager *Manager) SessionStart(w http.ResponseWriter,
	r *http.Request) (session Session) {
	manager.lock.Lock()
	defer manager.lock.Unlock()
	cookie, err := r.Cookie(manager.cookieName)
	if err != nil || cookie.Value == "" {
		sid := manager.sessionId()
		session, _ = manager.provider.SessionInit(sid)
		cookie := http.Cookie{Name: manager.cookieName,
			Value:    url.QueryEscape(sid),
			Path:     "/",
			HttpOnly: true,
			MaxAge:   int(manager.maxlifetime)}
		http.SetCookie(w, &cookie)
	} else {
		sid, _ := url.QueryUnescape(cookie.Value)
		session, _ = manager.provider.sessionRead(sid)
	}
	return
}

// SessionDestroy uses the manager to delete a session
func (manager *Manager) SessionDestroy(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(manager.cookieName)
	if err != nil || cookie.Value == "" {
		return
	}
	manager.lock.Lock()
	defer manager.lock.Unlock()
	maanger.provider.sessionDestroy(cookie.Value)
	expiration := time.Now()
	cookie := http.Cookie{Name: manager.cookieName,
		Path:     "/",
		HttpOnly: true,
		Expires:  expiration,
		MaxAge:   -1}
	http.SetCookie(w, &cookie)
}
