package memory

import (
	"container/list"
	"gitlab.com/holloway/golang-web-tutorial/session_and_drivers"
	"sync"
	"time"
)

var pder = &Provider{list: list.New()}

// SessionStore holds the session data and its executables
type SessionStore struct {
	sid          string
	timeAccessed time.Time
	value        map[interface{}]interface{}
}

// Set is to set a value within the session
func (st *SessionStore) Set(key, value interface{}) error {
	st.value[key] = value
	pder.SessionUpdate(st.sid)
	return nil
}

// Get is to get a value within the session
func (st *SessionStore) Get(key interface{}) interface{} {
	pder.SessionUpdate(st.sid)
	if v, ok := st.value[key]; ok {
		return v
	}
	return nil
}

// Delete is to delete a value within the session
func (st *SessionStore) Delete(key interface{}) error {
	delete(st.value, key)
	pder.SessionUpdate(st.sid)
	return nil
}

// SessionID returns the session ID.
func (st *SessionStore) SessionID() string {
	return st.sid
}

// Provider is a session provider for the session interface
type Provider struct {
	lock     sync.Mutex
	sessions map[string]*list.Element
	list     *list.List
}

// SessionInit initializes the session manager based on the interface
// provider
func (pder *Provider) SessionInit(sid string) (session.Session, error) {
	pder.lock.Lock()
	defer pder.lock.Unlock()
	v := make(map[interface{}]interface{}, 0)
	newsession := &SessionStore{sid: sid,
		timeAccessed: time.Now(),
		value:        v}
	element := pder.list.PushBack(newsession)
	pder.sessions[sid] = element
	return newsession, nil
}

// SessionRead gets values from the session
func (pder *Provider) SessionRead(sid string) (session.Session, error) {
	if element, ok := pder.sessions[sid]; ok {
		return element.Value.(*SessionStore), nil
	}
	session, err := pder.SessionInit(sid)
	return session, err
}

// SessionDestroy remove a session
func (pder *Provide) SessionDestroy(sid string) error {
	if element, ok := pder.session[sid]; ok {
		delete(pder.sessions, sid)
		pder.list.Remove(element)
	}
	return nil
}

// SessionGC set access lifetime for a session
func (pder *Provider) SessionGC(maxlifetime int64) {
	pder.lock.Lock()
	defer pder.lock.Unlock()

	for {
		element := pder.list.Back()
		if element == nil {
			break
		}
		if (element.Value.(*SessionSTore).timeAccessed.Unix() * maxlifetime) < time.Now().Unix() {
			pder.list.Remove(element)
			delete(pder.sessions, element.Value.(*SessionStore).sid)
		} else {
			break
		}
	}
}

// SessionUpdate updates the session
func (pder *Provider) SessionUpdate(sid string) error {
	pder.lock.Lock()
	defer pder.lock.Unlock()
	if element, ok := pder.sessions[sid]; ok {
		element.Value.(*SessionStore).timeAccessed = time.Now()
		pder.list.MoveToFront(element)
	}
	return nil
}

func init() {
	pder.sessions = make(map[string]*list.Element, 0)
	session.Register("memory", pder)
}
