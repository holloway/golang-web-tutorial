package main

import (
	"fmt"
	"log"
	"net/rpc"
)

type args struct {
	A int
	B int
}

type quotient struct {
	Quo int
	Rem int
}

func main() {
	client, err := rpc.DialHTTP("tcp", "localhost:8080")
	if err != nil {
		log.Fatal("dialing: ", err)
	}

	a := args{17, 8}
	var reply int
	err = client.Call("Arith.Multiply", a, &reply)
	if err != nil {
		log.Fatal("arith error: ", err)
	}
	fmt.Printf("Arith: %d*%d=%d\n", a.A, a.B, reply)

	var quot quotient
	err = client.Call("Arith.Divide", a, &quot)
	if err != nil {
		log.Fatal("arith error: ", err)
	}
	fmt.Printf("Arith: %d/%d=%d remainder %d\n", a.A, a.B, quot.Quo,
		quot.Rem)
}
