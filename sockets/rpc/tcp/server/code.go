package main

import (
	"errors"
	"fmt"
	"net"
	"net/rpc"
	"os"
)

// Argument is a structure holds the values of the argument
type Argument struct {
	A int
	B int
}

// Quotient is a structure holds the result of the division
type Quotient struct {
	Quo int
	Rem int
}

// Arith is a type of of interface offering Multiply and Divide
type Arith int

// Multiply performs multiplication from 2 numbers
func (t *Arith) Multiply(args *Argument, reply *int) error {
	*reply = args.A * args.B
	return nil
}

// Divide performs division between 2 numbers. Bears in mind
// that you should not provide divisor (B) as zero.
func (t *Arith) Divide(args *Argument, quo *Quotient) error {
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

func main() {
	a := new(Arith)
	rpc.Register(a)

	tcpAddr, err := net.ResolveTCPAddr("tcp", "localhost:1234")
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		rpc.ServeConn(conn)
	}
}

func checkError(err error) {
	if err != nil {
		fmt.Println("Fatal error ", err.Error())
		os.Exit(1)
	}
}
