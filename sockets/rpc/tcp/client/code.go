package main

import (
	"fmt"
	"log"
	"net/rpc"
)

// Argument is the structure that holds the argument values
type Argument struct {
	A int
	B int
}

// Quotient is the structure that holds the result of the division
type Quotient struct {
	Quo int
	Rem int
}

func main() {
	client, err := rpc.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("dialing: ", err)
	}

	// call
	args := Argument{17, 8}
	var reply int
	err = client.Call("Arith.Multiply", args, &reply)
	if err != nil {
		log.Fatal("arith error: ", err)
	}
	fmt.Printf("Arith: %d*%d=%d\n", args.A, args.B, reply)

	var quot Quotient
	err = client.Call("Arith.Divide", args, &quot)
	if err != nil {
		log.Fatal("arith error: ", err)
	}
	fmt.Printf("Arith: %d/%d=%d remainder %d\n", args.A, args.B, quot.Quo,
		quot.Rem)
}
