package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

func main() {
	service := ":1200"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		go handleClient(conn)
	}
}

func handleClient(conn net.Conn) {
	err := conn.SetReadDeadline(time.Now().Add(2 * time.Second))
	if err != nil {
		fmt.Printf("failed to set connection deadline")
	}
	request := make([]byte, 128) // max request length - flood attacks

	readLen, err := conn.Read(request)
	if err != nil {
		goto done
	}

	fmt.Printf("%s received: %v\n", conn.RemoteAddr(), request[:readLen])

	if readLen == 0 || string(request[:readLen]) == "close" {
		goto done
	} else if string(request[:readLen]) == "timestamp" {
		daytime := strconv.FormatInt(time.Now().Unix(), 10)
		_, err := conn.Write([]byte(daytime))
		if err != nil {
			fmt.Printf("failed to write")
		}
	} else {
		daytime := time.Now().String()
		_, err = conn.Write([]byte(daytime))
		if err != nil {
			fmt.Printf("failed to write")
		}
	}

done:
	closeConnection(conn)
}

func closeConnection(conn net.Conn) {
	err := conn.Close()
	if err != nil {
		fmt.Printf("failed to close connection")
	}
}

func checkError(err error) {
	if err != nil {
		fmt.Printf("Fatal error: %s\n", err.Error())
		os.Exit(1)
	}
}
