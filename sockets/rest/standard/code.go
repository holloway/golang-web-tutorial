package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// Article is a model holding an article data
type Article struct {
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

var articles = []Article{
	{Title: "First", Desc: "First Desc", Content: "First"},
	{Title: "Second", Desc: "Second Desc", Content: "Second"},
}

// libraries
func parseURL(pattern, url string) (map[string]string, error) {
	// trailing slash adjustment
	if string(pattern[len(pattern)-1:]) == "/" {
		pattern = pattern[:len(pattern)-1]
	}
	if string(url[len(url)-1:]) == "/" {
		url = url[:len(url)-1]
	}

	// string split
	patternParts := strings.Split(pattern, "/")
	urlParts := strings.Split(url, "/")

	// fail since the pattern is definitely not matching the url
	if len(patternParts) > len(urlParts) {
		err := errors.New("unmatched pattern")
		return nil, err
	}

	output := make(map[string]string)
	openSymbol := ":"
	element := ""
	keyword := ""
	checkStart := -1
	for index, part := range patternParts {
		part = strings.ToLower(part)
		element = urlParts[index]
		element = strings.ToLower(element)
		checkStart = strings.Index(part, openSymbol)
		if checkStart != 0 {
			if part != element {
				return nil, errors.New("unmatched pattern")
			}
			continue
		}
		keyword = strings.TrimLeft(part, openSymbol)
		output[keyword] = element
	}
	return output, nil
}

// viewModels + viewControllers?
func index(w http.ResponseWriter,
	r *http.Request,
	parsed map[string]string) {
	json.NewEncoder(w).Encode(articles)
}

func view(w http.ResponseWriter,
	r *http.Request,
	parsed map[string]string) {

	id, err := strconv.Atoi(parsed["aid"])
	if err != nil {
		log.Println("Articles: VIEW: invalid id: ", err)
		error400(w, r)
		return
	}

	if id < 0 || id > len(articles)-1 {
		log.Println("Articles: VIEW: invalid id: ", id)
		error400(w, r)
		return
	}
	json.NewEncoder(w).Encode(articles[id])
}

func createPage(w http.ResponseWriter,
	r *http.Request,
	parsed map[string]string) {

	type formCreate struct {
		Title   string `json:"Title"`
		Desc    string `json:"Desc"`
		Content string `json:"Content"`
	}

	decoder := json.NewDecoder(r.Body)
	var t formCreate
	err := decoder.Decode(&t)
	if err != nil {
		log.Println("Articles CREATE: json decoder: ", err)
		error400(w, r)
		return
	}
	defer r.Body.Close()

	article := Article{Title: t.Title,
		Desc:    t.Desc,
		Content: t.Content,
	}
	articles = append(articles, article)
	w.WriteHeader(204)
}

func update(w http.ResponseWriter,
	r *http.Request,
	params map[string]string) {
	id, err := strconv.Atoi(params["aid"])
	if err != nil {
		log.Println("fatal: ", err)
		error400(w, r)
		return
	}

	if id < 0 || id > len(articles)-1 {
		log.Println("Articles: UPDATE: fatal: invalid id: ", id)
		error400(w, r)
		return
	}

	type formUpdate struct {
		Title   string `json:"Title"`
		Desc    string `json:"Desc"`
		Content string `json:"Content"`
	}

	decoder := json.NewDecoder(r.Body)
	var t formUpdate
	err = decoder.Decode(&t)
	if err != nil {
		log.Println("Articles: UPDATE: decoder", err)
		error400(w, r)
		return
	}
	defer r.Body.Close()

	article := &articles[id]
	article.Title = t.Title
	article.Desc = t.Desc
	article.Content = t.Content

	w.WriteHeader(204)
}

func deletePage(w http.ResponseWriter,
	r *http.Request,
	params map[string]string) {
	id, err := strconv.Atoi(params["aid"])
	if err != nil {
		log.Println("Article DELETE: invalid id: ", err)
		error400(w, r)
		return
	}

	if id < 0 || id > len(articles)-1 {
		log.Println("Article DELETE: invalid id: ", id)
		error400(w, r)
		return
	}

	articles = append(articles[:id], articles[id+1:]...)

	w.WriteHeader(204)
}

func error400(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(400)
	fmt.Fprint(w, `"error": "it's a bad request. Check again."`)
}

func error404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	fmt.Fprint(w, `"error": "custom 404"`)
}

// controller
func restfulArticles(w http.ResponseWriter, r *http.Request) bool {
	params, _ := parseURL("/Articles", r.URL.Path)
	if params == nil {
		return false
	}

	switch r.Method {
	case "GET":
		// scan VIEW
		params, _ := parseURL("/Articles/:aid", r.URL.Path)
		if params != nil {
			view(w, r, params)
			return true
		}
		// scan INDEX
		index(w, r, params)
		return true
	case "POST":
		// scan CREATE
		createPage(w, r, params)
		return true
	case "PUT":
		// scan UPDATE
		params, _ := parseURL("/Articles/:aid", r.URL.Path)
		if params != nil {
			update(w, r, params)
			return true
		}
	case "DELETE":
		// scan DELETE
		params, _ := parseURL("/Articles/:aid", r.URL.Path)
		if params != nil {
			deletePage(w, r, params)
			return true
		}
	}
	return false
}

// main body
func urlRouter(w http.ResponseWriter, r *http.Request) {
	if restfulArticles(w, r) {
		return
	}
	error404(w, r)
}

func main() {
	http.HandleFunc("/", urlRouter)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
