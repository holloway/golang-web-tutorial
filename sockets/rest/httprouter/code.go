package main

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"strconv"
)

// models
type Article struct {
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

var articles = []Article{
	{Title: "First", Desc: "First Desc", Content: "First"},
	{Title: "Second", Desc: "Second Desc", Content: "Second"},
}

// viewModels
func index_page(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(articles)
}

func view_page(w http.ResponseWriter, r *http.Request, aid string) {

	id, err := strconv.Atoi(aid)
	if err != nil {
		log.Println("Articles: VIEW: invalid id: ", err)
		error_400_page(w, r)
		return
	}

	if id < 0 || id > len(articles)-1 {
		log.Println("Articles: VIEW: invalid id: ", id)
		error_400_page(w, r)
		return
	}
	json.NewEncoder(w).Encode(articles[id])
}

func create_page(w http.ResponseWriter, r *http.Request) {
	type form_create struct {
		Title   string `json:"Title"`
		Desc    string `json:"Desc"`
		Content string `json:"Content"`
	}

	decoder := json.NewDecoder(r.Body)
	var t form_create
	err := decoder.Decode(&t)
	if err != nil {
		log.Println("Articles CREATE: json decoder: ", err)
		error_400_page(w, r)
		return
	}
	defer r.Body.Close()

	article := Article{Title: t.Title,
		Desc:    t.Desc,
		Content: t.Content,
	}
	articles = append(articles, article)
	w.WriteHeader(204)
}

func update_page(w http.ResponseWriter, r *http.Request, aid string) {
	id, err := strconv.Atoi(aid)
	if err != nil {
		log.Println("fatal: ", err)
		error_400_page(w, r)
		return
	}

	if id < 0 || id > len(articles)-1 {
		log.Println("Articles: UPDATE: fatal: invalid id: ", id)
		error_400_page(w, r)
		return
	}

	type form_update struct {
		Title   string `json:"Title"`
		Desc    string `json:"Desc"`
		Content string `json:"Content"`
	}

	decoder := json.NewDecoder(r.Body)
	var t form_update
	err = decoder.Decode(&t)
	if err != nil {
		log.Println("Articles: UPDATE: decoder", err)
		error_400_page(w, r)
		return
	}
	defer r.Body.Close()

	article := &articles[id]
	article.Title = t.Title
	article.Desc = t.Desc
	article.Content = t.Content

	w.WriteHeader(204)
}

func delete_page(w http.ResponseWriter, r *http.Request, aid string) {
	id, err := strconv.Atoi(aid)
	if err != nil {
		log.Println("Article DELETE: invalid id: ", err)
		error_400_page(w, r)
		return
	}

	if id < 0 || id > len(articles)-1 {
		log.Println("Article DELETE: invalid id: ", id)
		error_400_page(w, r)
		return
	}

	articles = append(articles[:id], articles[id+1:]...)
	w.WriteHeader(204)
}

func error_400_page(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(400)
	fmt.Fprint(w, `"error": "it's a bad request. Check again."`)
}

func error_404_page(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	fmt.Fprint(w, `"error": "custom 404"`)
}

// controller
func get_articles(w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params) {
	// Scan VIEW
	aid := params.ByName("aid")
	fmt.Println(params.ByName("cid"))
	if aid != "" {
		view_page(w, r, aid)
		return
	}

	// Fall to INDEX
	index_page(w, r)
}

func post_articles(w http.ResponseWriter,
	r *http.Request,
	_ httprouter.Params) {
	create_page(w, r)
}

func put_articles(w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params) {
	aid := params.ByName("aid")
	if aid != "" {
		update_page(w, r, aid)
		return
	}
}

func delete_articles(w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params) {
	aid := params.ByName("aid")
	if aid != "" {
		delete_page(w, r, aid)
		return
	}
}

func register_articles(router *httprouter.Router) {
	router.GET("/Articles", get_articles)
	router.GET("/Articles/:aid", get_articles)
	router.GET("/Articles/:aid/comments/:cid", get_articles)
	router.POST("/Articles", post_articles)
	router.PUT("/Articles/:aid", put_articles)
	router.DELETE("/Articles/:aid", delete_articles)
}

// main
func main() {
	router := httprouter.New()
	register_articles(router)
	log.Fatal(http.ListenAndServe(":8080", router))
}
