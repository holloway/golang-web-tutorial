#!/bin/bash

id="0"
json_data="\
{
	\"title\": \"Modified New Title\",
	\"desc\": \"Modified New Description\",
	\"content\": \"Modified New Content\"
}"
url="http://localhost:8080/articles/$id"

curl -X PUT -L \
	-H "Content-Type: application/json" \
	-d "$json_data" \
	"$url"

