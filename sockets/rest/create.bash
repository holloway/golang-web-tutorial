#!/bin/bash

json_data="\
{
	\"title\": \"Created New Title\",
	\"desc\": \"Created New Description\",
	\"content\": \"Created New Content\"
}"

curl -X POST -L\
	-H "Content-Type: application/json" \
	-d "$json_data" \
	http://localhost:8080/articles

