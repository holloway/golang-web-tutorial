#!/bin/bash

install() {
	sudo apt-get install postgresql -y
	sudo passwd postgres
	read -sp "Postgresql admin password: " admin_pass
	su - postgres -c "psql -U postgres -d postgres -c \"alter user postgres with password '$admin_pass';\""
}

setup() {
	echo "switching user postgres"
	su - postgres -c "psql -U postgres -d postgres -c \"\
CREATE TABLE userinfo
(
	uid serial NOT NULL,
	username character varying(100) NOT NULL,
	departname character varying(500) NOT NULL,
	Created date,
	CONSTRAINT userinfo_pkey PRIMARY KEY (uid)
)
WITH (OIDS=FALSE);
\""
}


delete() {
	echo "Switching user: postgres"
	su - postgres -c "psql -U postgres -d postgres -c \"DROP TABLE userinfo;\""
}


main() {
	case $1 in
	"-i")
		install
		;;
	"-s")
		setup
		;;
	"-d")
		delete
		;;
	*)
		;;
	esac
}

main $@
