package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"time"
)

const (
	DB_USER     = "postgres"
	DB_PASSWORD = "testing123"
	DB_NAME     = "postgres"
)

func main() {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER,
		DB_PASSWORD,
		DB_NAME)
	db, err := sql.Open("postgres", dbinfo)
	checkErr(err)
	defer db.Close()

	//insert
	var lastInsertId int
	err = db.QueryRow("INSERT INTO "+
		"userinfo(username,departname,created) "+
		"VALUES($1,$2,$3) "+
		"returning uid;",
		"astaxie",
		"研发部门",
		"2012-12-09").Scan(&lastInsertId)
	checkErr(err)
	fmt.Println("last inserted id =", lastInsertId)

	//update
	stmt, err := db.Prepare("UPDATE userinfo set username=$1 where uid=$2")
	checkErr(err)
	res, err := stmt.Exec("astaxieupdate", lastInsertId)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	fmt.Println(affect, "rows changed")

	// querying
	rows, err := db.Query("SELECT * FROM userinfo")
	checkErr(err)

	var uid int
	var username string
	var department string
	var created time.Time
	for rows.Next() {
		err = rows.Scan(&uid, &username, &department, &created)
		checkErr(err)
		fmt.Println("uid | username | department | created")
		fmt.Printf("%3v | %8v | %6v | %6v\n",
			uid,
			username,
			department,
			created)
	}

	// deleting
	stmt, err = db.Prepare("DELETE FROM userinfo WHERE uid=$1")
	checkErr(err)

	res, err = stmt.Exec(lastInsertId)
	checkErr(err)

	affect, err = res.RowsAffected()
	checkErr(err)

	fmt.Println(affect, "rows changed")
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
