#!/bin/bash

user="root"
password="password"
database="holloway"

mac_install() {
	brew install mysql
	mysqld --initialize
}

ubuntu_install() {
	sudo apt-get install mysql-server -y
	sudo echo "\
[client]
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
" >> /etc/mysql/my.cnf
	sudo /etc/init.d/mysql restart
}

create_table() {
	mysql -u "$user" -p"$password" $database -e "\
CREATE TABLE \`userinfo\` (
\`uid\` INT(10) NOT NULL AUTO_INCREMENT,
\`username\` VARCHAR(64) NULL DEFAULT NULL,
\`departname\` VARCHAR(64) NULL DEFAULT NULL,
\`created\` DATE NULL DEFAULT NULL,
PRIMARY KEY (\`uid\`)
);"
}

delete_table() {
	mysql -u "$user" -p"$password" $database -e "DROP TABLE userinfo;"
}

case $1 in
	-mi)
		mac_install
		;;
	-c)
		create_table
		;;
	-d)
		delete_table
		;;
	*)
		;;
esac
