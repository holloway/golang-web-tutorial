#!/bin/bash
install() {
	cloud_sdk_repo="cloud-sdk-$(lsb_release -c -s)"
	echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | \
sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
	curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | \
sudo apt-key add -
	sudo apt update -y
	sudo apt install google-cloud-sdk -y
	sudo apt install google-cloud-sdk-app-engine-go -y
	sudo apt install google-cloud-sdk-datastore-emulator -y
	gcloud init
}

main() {
	install
}
main $@
