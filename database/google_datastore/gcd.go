package main

import (
	"cloud.google.com/go/datastore"
	"context"
	"fmt"
	"os"
)

var (
	projectID = os.Getenv("DATASTORE_PROJECT_ID")
)

const (
	entity = "Book"
)

// Book is the structure holding the metadata of a published book
type Book struct {
	Key	*datastore.Key
	ISBN	string		`json:"isbn"`
	Title	string		`json:"title"`
	Author	[]string	`json:"authors"`
	Price	string		`json:"price"`
}

// Index gets all to books and populate them
func Index() ([]Book, error) {
	var books []Book

	ctx := context.Background()
	client, err := datastoreClient()
	if err != nil {
		return nil, err
	}

	query := datastore.NewQuery(entity).Order("Title")
	_ , err = client.GetAll(ctx, query, &books)
	if err != nil {
		return nil, err
	}
	return books, nil
}

// Get is a function to get a particular book
func Get(id string) (*Book, error) {
	var book Book

	ctx := context.Background()
	client, err := datastoreClient()
	if err != nil {
		return nil, err
	}

	key, err := datastore.DecodeKey(id)
	if err != nil {
		return nil, err
	}

	err = client.Get(ctx, key, &book)
	if err != nil {
		return nil, err
	}
	return &book, nil
}

// Create is a function to create an entry for a new book
func Create(book *Book) error {
	ctx := context.Background()
	client, err := datastoreClient()
	if err != nil {
		return err
	}

	key := datastore.NameKey(entity, book.ISBN, nil)
	book.Key = key
	_, err = client.Put(ctx, key, book)
	return err
}

// Update is a function to update a book entry with new data
func Update(id string, newData Book) error {
	ctx := context.Background()
	client, err := datastoreClient()
	if err != nil {
		return err
	}

	book, err := Get(id)
	if err != nil {
		return err
	}

	book.ISBN = newData.ISBN
	book.Title = newData.Title
	book.Author = newData.Author
	book.Price = newData.Price
	_, err = client.Put(ctx, book.Key, book)
	return err
}

// Delete is a function to remove a book from the database
func Delete(id string) error {
	key, err := datastore.DecodeKey(id)
	if err != nil {
		return err
	}

	client, err := datastoreClient()
	if err != nil {
		return err
	}

	ctx := context.Background()
	err = client.Delete(ctx, key)
	return err
}

func datastoreClient() (*datastore.Client, error) {
	ctx := context.Background()
	return datastore.NewClient(ctx, projectID)
}

func main() {
	var err error

	books := []Book{
		Book {
			ISBN: "0134190440",
			Title: "The Go Programming Language",
			Author: []string {
				"Alan A. A. Donovan",
				"Brian W. Kernighan",
			},
			Price: "$34.57",
		},
	}

	for _, book := range books {
		err = Create(&book)
		if err != nil {
			fmt.Printf("CREATE error: %v\n", err)
		}
	}

	// index the entities
	index, err := Index()
	fmt.Printf("Index: %v, Error: %v\n", index, err)

	// create a book
	book1 := Book {
		ISBN: "0134190441",
		Title: "The Go Programming Language",
		Author: []string {
			"Alan A. A. Donovan",
			"Brian W. Kernighan",
		},
		Price: "$20.00",
	}
	err = Create(&book1)
	if err != nil {
		fmt.Printf("CREATE Error: %v\n", err)
	}

	index, err = Index()
	fmt.Printf("Index: %v, Error: %v\n", index, err)

	// get key and use it
	key := index[len(index)-1].Key.Encode()
	fmt.Printf("Key for Index[-1]: %s\n", key)
	book2, err := Get(key)
	fmt.Printf("book2: %v\n, Error:%v\n", book2, err)

	// update book
	newBook := Book {
		ISBN: book2.ISBN,
		Title: "The Python Programming Language",
		Author: []string {
			"John Doe",
			"Mary Doe",
		},
		Price: "$12.00",
	}
	err = Update(key, newBook)
	if err != nil {
		fmt.Printf("UPDATE error: %v\n", err)
	}

	index, err = Index()
	fmt.Printf("Index: %v, Error: %v\n", index, err)

	// delete book
	var id string
	for _, book := range index {
		id = book.Key.Encode()
		err = Delete(id)
		if err != nil {
			fmt.Printf("DELETE error: %v\n", err)
		}
	}

	index, err = Index()
	fmt.Printf("Index: %v, Error: %v \n", index, err)
}
