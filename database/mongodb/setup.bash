#!/bin/bash

create_admin() {
	username=${1:-root}
	password=${2:-$(openssl rand -base64 32)}
	database=${3:-admin}

	if [[ "$database" != "admin" ]]; then
		role=${MONGODB_ROLE:-dbOwner}
	else
		role=${MONGODB_ROLE:-dbAdminAnyDatabase}
	fi

	mongo $database -u 'root' -p 'password' --eval "\
db.createUser({\
user: '$username', \
pwd: '$password', \
roles: [{ role: '$role', db: '$database'}]\
});"
	echo "-----------------------------"
	echo "username: $username"
	echo "password: $password"
	echo "database: $database"
	echo "role: $role"
	echo "-----------------------------"
}

secure() {
	filepath="/etc/mongodb.conf"
	line=$(grep -n '#auth = true' $filepath | cut -d: -f 1)
	if [ "$line" != "" ]; then
		sudo sed -i "${line}s|^.*$|auth = true|" $filepath
	fi
	sudo /etc/init.d/mongodb restart
}

repair() {
	sudo mkdir -p /data/db
	sudo chmod +w /data
	sudo /etc/init.d/mongodb stop
	sudo rm /var/lib/mongodb/mongod.lock
	mongod --repair
	sudo /etc/init.d/mongodb start
	while ! nc -vz localhost 27017; do sleep 1; done
}

install() {
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.ciom:80 \
		--recv 7F0CEB10
	sudo echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
	sudo apt-get update
	sudo apt-get install mongodb-org -y --allow-unauthneticated
}

main() {
	install
	repair
	create_admin
	create_admin "root" "password" "store"
	secure
}

