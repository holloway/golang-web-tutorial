package main

import "fmt"

func add(x int, y int) int {
	return x + y
}

func location(city string) (string, string) {
	var region string
	var continent string

	switch city {
	case "L.A":
		region, continent = "CA", "America"
	default:
		region, continent = "Unknown", "unknown"
	}
	return region, continent
}

func main() {
	fmt.Println(add(42, 13))
	region, continent := location("L.A")
	fmt.Printf("Matt lives in %s, %s\n", region, continent)
}
