/*
Interface is like struct for functions. It binds with the data struct available
at the given time.

Example, Human SayHi() and Sing() automatically binds with human struct within
the derrived structs.
*/

package main

import (
	"fmt"
)

type Human struct {
	name  string
	age   int
	phone string
}

type Student struct {
	Human  // human structure
	school string
	loan   float32
}

type Employee struct {
	Human   // human structure
	company string
	money   float32
}

// a Human method that says Hi
func (h Human) SayHi() {
	fmt.Printf("Hi, I'm %s. You can call me on %s\n", h.name, h.phone)
}

// a Human method that sing
func (h Human) Sing(lyric string) {
	fmt.Println("La la la la.....", lyric)
}

// employee's method overriding says Hi
func (e Employee) SayHi() {
	fmt.Printf("Hi, I am %s, I work at %s. Call me on %s\n", e.name,
		e.company,
		e.phone)
}

// building an interface using the functions
type Men interface {
	SayHi()
	Sing(lyric string)
}

func main() {
	mike := Student{Human{"Mike", 25, "212-222-XXX"}, "MIT", 0.00}
	paul := Student{Human{"Paul", 26, "123-231-XXX"}, "Harvard", 100}
	sam := Employee{Human{"Sam", 36, "444-123-XXX"}, "GoLang Inc.", 1000}
	tom := Employee{Human{"Sam", 36, "444-123-XXX"}, "Things. Ltd", 5000}

	var i Men
	i = mike
	fmt.Println("This is Mike. A Student:")
	i.SayHi()
	i.Sing("November Rain")
	/* OUTPUT:
	This is Mike, a Student:
	Hi, I am Mike you can call me on 222-222-XXX
	La la la la... November rain
	*/

	i = tom
	fmt.Println("This is Tom, an Employee:")
	i.SayHi()
	i.Sing("Born to be wild")
	/* OUTPUT:
	This is Tom, an Employee:
	Hi, I am Sam, I work at Things Ltd.. Call me on 444-222-XXX
	La la la la... Born to be wild
	*/

	//a slice of Men
	fmt.Println("Let's use a slice of Men and see what happens")
	x := make([]Men, 3)
	x[0], x[1], x[2] = paul, sam, mike

	for _, value := range x {
		value.SayHi()
	}

	/* OUTPUT:
	Let’s use a slice of Men and see what happens
	Hi, I am Paul you can call me on 111-222-XXX
	Hi, I am Sam, I work at Golang Inc.. Call me on 444-222-XXX
	Hi, I am Mike you can call me on 222-222-XXX
	*/
}
