package main

import (
	"fmt"
	"time"
)

func basicArray() {
	var a [10]int
	a[0] = 0
	a[1] = 1
	a[2] = 2
	a[3] = 3
	a[4] = 4
	a[5] = 5
	a[6] = 6
	a[7] = 7
	a[8] = 8
	a[9] = 9
	b := [2]string{"hello", "world"}
	c := [...]string{"no", "matter", "how", "long", "this", "array", "is"}
	fmt.Println(a[1])
	fmt.Println(b[0])
	fmt.Println(c[0])
}

func clusteredSplice() {
	start := time.Now()

	cities := []string{
		"Santa Monica",
		"San Diego",
		"San Francisco",
	}
	fmt.Println("city: ", cities[0])

	elapsed := time.Since(start)
	fmt.Println("Time taken ", elapsed)
}

func appendSplice() {
	start := time.Now()

	cities := []string{}
	cities = append(cities, "Stana Monica", "Sna Diego", "Snd Francisco")
	fmt.Println("city: ", cities[0])
	elapsed := time.Since(start)
	fmt.Println("Time taken: ", elapsed)
}

func rangeSplice() {
	var pow = []int{1, 2, 4, 8, 16, 32, 64, 128}
	for i, v := range pow {
		fmt.Printf("2**%d = %d\n", i, v)
	}
}

func mapSplice() {
	cities := map[string]int{
		"New York":    8336697,
		"Los Angeles": 3857799,
		"Chicago":     2714856,
	}

	// get value from map
	population := cities["New York"]
	fmt.Printf("%d \n", population)

	// update value for a key
	cities["New York"] = 6767676767

	// delete element
	delete(cities, "Chicago")

	// test element
	elem, ok := cities["Chicago"]
	fmt.Printf("%d %t \n", elem, ok)
	elem, ok = cities["New York"]
	fmt.Printf("%d %t \n", elem, ok)

	// run through each print
	for key, value := range cities {
		fmt.Printf("%s has %d inhabitants\n", key, value)
	}
	fmt.Printf("%#v \n", cities)
}

func exercise() {
	var names = []string{"Katrina", "Evan", "Neil",
		"Adam", "Martin", "Matt",
		"Emma", "Isabella", "Emily",
		"Madison", "Ava", "Olivia",
		"Sophia", "Abigail", "Elizabeth",
		"Chloe", "Samantha", "Addison",
		"Natalie", "Mia", "Alexis"}
	var maxLength = 1
	for _, name := range names {
		if len(name) > maxLength {
			maxLength = len(name)
		}
	}

	output := make([][]string, maxLength)
	for _, name := range names {
		output[len(name)-1] = append(output[len(name)-1], name)
	}

	fmt.Printf("%v", output)
}

func main() {
	basicArray()
	clusteredSplice()
	appendSplice()
	rangeSplice()
	mapSplice()
	exercise()
}
