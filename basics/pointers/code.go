package main

import (
	"fmt"
	"http"
)

func main() {
	client := &http.Client{}
	resp, err := client.Get("http://gobootcamp.com")
	fmt.Println(resp, err)
}

// NOTE:
// GO doesn't have pointer-arithmetic. It doesn't work in term of incremental
// memory address, like in DMA.
