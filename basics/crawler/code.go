package main

import (
	"fmt"
	"sync"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

type Data struct {
	body   string
	urls   []string
	errors error
}

type Cache struct {
	data map[string]Data
	mux  sync.Mutex
}

var caches = Cache{
	data: make(map[string]Data),
}

func cache(url, body *string, err *error, urls *[]string) {
	caches.mux.Lock()
	caches.data[*url] = Data{
		body:   *body,
		urls:   *urls,
		errors: *err,
	}
	caches.mux.Unlock()
}

func loadCache(url *string) (string, []string, error) {
	caches.mux.Lock()
	val := caches.data[*url]
	defer caches.mux.Unlock()
	return val.body, val.urls, val.errors
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func delegateCrawl(url string, depth int, ch chan string) {
	var body string
	var err error
	var urls []string

	if _, ok := caches.data[url]; ok {
		body, urls, err = loadCache(&url)
	} else {
		ch <- fmt.Sprintf("[NOTE] Not cached. Fetching %s\n", url)
		body, urls, err = fetcher.Fetch(url)
	}

	if _, ok := caches.data[url]; !ok {
		cache(&url, &body, &err, &urls)
	}

	if err != nil {
		ch <- fmt.Sprintln(err)
	} else {
		ch <- fmt.Sprintf("found: %s %q\n", url, body)
	}

	if depth > 0 {
		for _, u := range urls {
			go delegateCrawl(u, depth-1, ch)
		}
	} else {
		ch <- "stop"
	}
}

func Crawl(url string, depth int, fetcher Fetcher) {
	ch := make(chan string)
	go delegateCrawl(url, depth, ch)
	for {
		select {
		case message := <-ch:
			if message == "stop" {
				close(ch)
				return
			}
			fmt.Printf(message)
		default:
		}
	}
}

func main() {
	Crawl("http://golang.org/", 4, fetcher)
}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"http://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"http://golang.org/pkg/",
			"http://golang.org/cmd/",
		},
	},
	"http://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"http://golang.org/",
			"http://golang.org/cmd/",
			"http://golang.org/pkg/fmt/",
			"http://golang.org/pkg/os/",
		},
	},
	"http://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
	"http://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
}
