package models

// list of imports go first
import (
	"fmt"
)

// constants go seconds
const (
	ConstExample = "constant before variables"
)

// variables go third
var (
	ExportedVar    = 12
	nonExportedVar = 12
)

// data structure and interface strunctures in fourth
type userLocation struct {
	City, Country string
}

type user struct {
	FirstName, LastName string
	Location            *userLocation
}

/*
TIPS:
1) Use 'type' to define a new data type you don't own. Observe the 'User' type
   above and used below.
*/

// function in fifth
func newUser(firstName, lastName string) *user {
	return &User{
		FirstName: firstName,
		LastName:  lastName,
		Location: &UserLocation{
			City:    "Santa Monica",
			Country: "USA",
		},
	}
}

// method in the last
func (u *user) greeting() string {
	return fmt.Sprintf("Dear %s %s", u.FirstName, u.LastName)
}
