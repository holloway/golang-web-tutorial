package main

import (
	"fmt"
)

func main() {
	name := "Hello World"
	fmt.Printf("The variable data is %s\n", name)
	number := "   123457"
	fmt.Printf("The number is %s\n", number)
}
