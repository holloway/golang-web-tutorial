package main

import (
	"fmt"
	"sync"
	"time"
)

// structure with mutex lock
type SafeCounter struct {
	v   map[string]int
	mux sync.Mutex
}

// method to increment the counter
func (c *SafeCounter) Inc(key string) {
	// lock first
	c.mux.Lock()
	c.v[key]++
	c.mux.Unlock()
}

// return value of current key
func (c *SafeCounter) Value(key string) int {
	c.mux.Lock()
	defer c.mux.Unlock()
	return c.v[key]
}

func main() {
	c := SafeCounter{v: make(map[string]int)}
	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}

	time.Sleep(time.Second)
	fmt.Println(c.Value("somekey"))
}
