package main

import "fmt"

type box struct {
	width  float64
	height float64
}

func main() {
	b1 := new(box)
	b1.width = 15
	b1.height = 20
	fmt.Println("The box width is:", b1.width)
}
