package main

import "fmt"

func main() {
	fmt.Println("counting")

	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	/* this will print 9-0 instead of 0-9 */
	fmt.Println("done")
}
