package main

import (
	"fmt"
	"time"
)

func sum(a []int, c chan int) {
	sum := 0
	for _, v := range a {
		sum += v
	}
	c <- sum // send sum to channel
}

func singleDirectionChannelSend() {
	a := []int{7, 2, 8, -9, 4, 0}
	c := make(chan int, 2) // create channel, with buffer up to 2

	go sum(a[:len(a)/2], c)
	go sum(a[len(a)/2:], c)
	x, y := <-c, <-c // receive from channel
	fmt.Println(x, y, x+y)
	close(c)
}

func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for {
		select { // select waits for channel available data
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		case <-time.After(15 * time.Millisecond): // adding timeout
			fmt.Println("Timeout man!")
			return
		default:
			fmt.Println("waiting for data from any channel cases")
		}
	}
}

func fibonacciMain() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	fibonacci(c, quit)
}

// to make sure things are running
func main() {
	singleDirectionChannelSend()
	fibonacciMain()
}
