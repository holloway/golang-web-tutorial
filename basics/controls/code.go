package main

import (
	"fmt"
)

func exercise() {
	var (
		coins = 50
		users = []string{
			"Matthew", "Sarah", "Augustus", "Heidi", "Emilie",
			"Peter", "Giana", "Adriano", "Aaron", "Elizabeth",
		}
		distribution = make(map[string]int, len(users))
		i            int
	)

	for _, user := range users {
		distribution[user] = 0
		for i = 0; i < len(user); i++ {
			switch string(user[i]) {
			case "a", "A":
				distribution[user]++
			case "e", "E":
				distribution[user]++
			case "i", "I":
				distribution[user] += 2
			case "o", "O":
				distribution[user] += 3
			case "u", "U":
				distribution[user] += 4
			}
		}
		if distribution[user] > 10 {
			distribution[user] = 10
		}
	}

	fmt.Println(distribution)
	fmt.Println("Coins left:", coins)
}

func main() {
	var i int
	logicKey := true

	// if else loop
	if logicKey == false && logicKey != true {
		fmt.Printf("It's impossible to print this line.\n")
	} else if logic_key == true || logic_key != false {
		fmt.Printf("This is the actual printing.\n")
	} else {
		fmt.Printf("Something went seriously wrong!\n")
	}

	// switch cases - breaks automatically for each case
	switch logicKey {
	case true, !false:
		fmt.Printf("True cases\n")
	case false:
		fmt.Printf("False cases\n")
	default:
		fmt.Printf("Error man!\n")
	}

	// switch - execute each cases by fallthrough, C-style
	switch logicKey {
	case false:
		fmt.Printf("fall through false case\n")
		fallthrough
	case true:
		fmt.Printf("fall through true case\n")
		fallthrough
	case !false:
		fmt.Printf("fall through not false case\n")
		if logic_key != true {
			// break the fall if needed
			break
		}
		fallthrough
	case !true:
		fmt.Printf("fall through not true case\n")
		fallthrough
	default:
		fmt.Printf("Try again man!\n")
	}

	// for looping
	sum := 1
	for i = 0; i < 10; i++ {
		sum += i
		fmt.Printf("%d\n", sum)
	}

	// conditional looping (switches)
	sum = 1
	for sum <= 100 {
		sum += sum
		fmt.Printf("%d\n", sum)
	}

	// infinity looping
	for {
		fmt.Printf("To infinity and beyond!\n")
	}
}
