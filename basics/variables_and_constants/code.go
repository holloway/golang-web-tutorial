/*
Variables
*/
package main

import (
	"fmt"
)

var (
	name            string
	location        string
	integer         int
	zone            float32
	unsignedInteger uint

	unsignedIntegerBy8Bit  uint8
	unsignedIntegerBy16Bit uint16
	unsignedIntegerBy32Bit uint32
	unsignedIntegerBy64Bit uint64
	integerBy8Bit          int8
	integerBy16Bit         int16
	integerBy32Bit         int32
	integerBy64Bit         int64
	floatBy32Bit           float32
	floatBy64Bit           float64
	complexByFloat32       complex64
	complexByFloat64       complex128
	pointer                string

	// GlobalController is a variable holding string
	GlobalController string
)

const (
	pi = 3.142
)

func main() {
	// := only works within a function
	name, location := "Hello", "World"
	fmt.Println(name, location)

	integer = 9
	zone = 9.5353
	unsignedInteger = 240
	unsignedIntegerBy8Bit = 0xFF
	unsignedIntegerBy16Bit = 0xFFFF
	unsignedIntegerBy32Bit = 0xFFFFFFFF
	unsignedIntegerBy64Bit = 0xFFFFFFFFFFFFFFFF
	integerBy8Bit = 127
	integerBy16Bit = 32767
	integerBy32Bit = 214748367
	integerBy64Bit = 9223372036854775807
	floatBy32Bit = 10e2
	floatBy64Bit = 10e4
	complexByFloat32 = 0.52 + 8i
	complexByFloat64 = 0.235 + 12i
	pointer = name
	GlobalController = "Hello World"
}
