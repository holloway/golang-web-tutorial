package main

import (
	"encoding/json"
	"os"
)

type server struct {
	// ID wil not be outputted
	ID int `json:"-"`

	// ServerName2 will be converted to json type.
	ServerName  string `json:"serverName"`
	ServerName2 string `json:"serverName2,string"`

	// If ServerIP is empty, it will not be outputted
	ServerIP string `json:"serverIP,omitempty"`
}

func main() {
	s := server{
		ID:          3,
		ServerName:  `Go "1.0" `,
		ServerName2: `Go "1.1" `,
		ServerIP:    ``,
	}

	b, _ := json.Marshal(s)
	os.Stdout.Write(b)
}
