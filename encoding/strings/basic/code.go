package main

import (
	"bytes"
	"fmt"
	"strings"
)

func scan_substring() {
	fmt.Println(strings.Contains("seafood", "foo"))
	fmt.Println(strings.Contains("seafood", "bar"))
	fmt.Println(strings.Contains("seafood", ""))
	fmt.Println(strings.Contains("", ""))
}

func dynamic_string() {
	var buffer bytes.Buffer

	// chaotic append or handling
	for i := 0; i < 1000; i++ {
		buffer.WriteString("a")
	}

	fmt.Println(buffer.String())
}

func join_string() {
	s := []string{"foo", "bar", "baz"}
	fmt.Println(strings.Join(s, ", "))
}

func find_substring() {
	fmt.Println(strings.Index("chicken", "ken"))
	fmt.Println(strings.Index("chicken", "dmr"))
}

func repeat_substring() {
	fmt.Println("ba" + strings.Repeat("na", 2))
}

func replace_substring() {
	fmt.Println(strings.Replace("oink oink oink", "k", "ky", 2))
	fmt.Println(strings.Replace("oink oink oink", "oink", "moo", -1))
}

func split_string_into_array() {
	fmt.Printf("%q\n", strings.Split("a,b,c", ","))
	fmt.Printf("%q\n", strings.Split("a man a plan a canal panama", "a "))
	fmt.Printf("%q\n", strings.Split(" xyz ", ""))
	fmt.Printf("%q\n", strings.Split("", "Bernardo O'Higgins"))
}

func remove_substring() {
	fmt.Printf("[%q]", strings.Trim(" !!! Achtung !!! ", "! "))
}

func strip_space_into_array() {
	fmt.Printf("Fields are: %q", strings.Fields("  foo bar    baz      "))
}

func main() {
	scan_substring()
	dynamic_string()
	join_string()
	find_substring()
	repeat_substring()
	replace_substring()
	split_string_into_array()
	remove_substring()
	strip_space_into_array()
}
