package main

import (
	"html/template"
	"os"
)

type friend struct {
	Fname string
}

type person struct {
	UserName string
	Emails   []string
	friends  []*Friend
}

func main() {
	f1 := friend{Fname: "minux.ma"}
	f2 := friend{Fname: "xushiwei"}
	t := template.New("fieldname example")
	t, _ = t.Parse(`hello {{.UserName}}!
		{{range .Emails}}
			an email {{.}}
		{{end}}
		{{with .friends}}
		{{range .}}
			my friend name is {{.Fname}}
		{{end}}
		{{end}}
` + "\n")
	p := person{UserName: "Astaxie",
		Emails:  []string{"astaxie@beego.me", "astaxie@gmail.com"},
		friends: []*Friend{&f1, &f2}}
	t.Execute(os.Stdout, p)
}
