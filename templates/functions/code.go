package main

import (
	"fmt"
	"html/template"
	"os"
	"strings"
)

type friend struct {
	Fname string
}

type person struct {
	UserName string
	Emails   []string
	friends  []*friend
}

func emailDealWith(args ...interface{}) string {
	ok := false
	var s string
	if len(args) == 1 {
		s, ok = args[0].(string)
	}
	if !ok {
		s = fmt.Sprint(args...)
	}
	// find the @ symbol
	substrs := strings.Split(s, "@")
	if len(substrs) != 2 {
		return s
	}
	// replace the @ by " at "
	return (substrs[0] + " at " + substrs[1])
}

func main() {
	f1 := friend{Fname: "minux.ma"}
	f2 := friend{Fname: "xushiwei"}
	t := template.New("fieldname example")
	t = t.Funcs(template.FuncMap{"emailDeal": emailDealWith})
	t, _ = t.Parse(`hello {{.UserName}}!
		{{range .Emails}}
			an emails {{.|emailDeal}}
		{{end}}
		{{with .friends}}
		{{range .}}
			my friend name is {{.Fname}}
		{{end}}
		{{end}}
		`)
	p := person{UserName: "Astaxie",
		Emails: []string{"astaxie@beego.me",
			"astaxie@gmail.com"},
		friends: []*friend{&f1, &f2}}
	t.Execute(os.Stdout, p)
}
