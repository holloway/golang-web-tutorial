package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

var templates *template.Template

func main() {
	var allFiles []string
	directoryPath := "./html"
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo,
		err error) error {
		_, filename := filepath.Split(path)
		if strings.HasSuffix(filename, ".tmpl") {
			allFiles = append(allFiles, path)
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(allFiles)

	templates, _ = template.ParseFiles(allFiles...)
	content := templates.Lookup("content.tmpl")
	content.ExecuteTemplate(os.Stdout, "content", nil)
}
