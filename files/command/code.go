package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
)

func simple() {
	output, err := exec.Command("ls", ".").Output()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error %v\n", err)
	}
	fmt.Println(string(output))
}

func stream() {
	cmd := exec.Command("ls", ".")
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("print: %s\n", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting command", err)
		os.Exit(1)
	}

	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for command", err)
		os.Exit(1)
	}
}

func main() {
	stream()
}
