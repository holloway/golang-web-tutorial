package main

import (
	"fmt"
	"os"
)

func main() {
	userFile := "astaxie.txt"

	// write
	fout, err := os.Create(userFile)
	if err != nil {
		fmt.Println(userFile, err)
	}
	defer fout.Close()
	for i := 0; i < 10; i++ {
		fout.WriteString("Just a test!\r\n")
		fout.Write([]byte("Just a test!\r\n"))
	}

	// read
	fl, err := os.Open(userFile)
	if err != nil {
		fmt.Println(userFile, err)
		return
	}
	defer fl.Close()
	buf := make([]byte, 1024)
	for {
		n, _ := fl.Read(buf)
		if 0 == n {
			break
		}
		os.Stdout.Write(buf[:n])
	}

	// delete
	os.Remove(userFile)
}
