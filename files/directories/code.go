package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func main() {
	// $ mkdir
	err := os.Mkdir("./astaxie", 0700)
	if err != nil {
		fmt.Println(err)
	}

	// $ mkdir -p
	err = os.MkdirAll("./astaxie/test1/test2", 0700)
	if err != nil {
		fmt.Println(err)
	}

	// $ rm -r
	err = os.Remove("./astaxie")
	if err != nil {
		fmt.Println(err)
	}

	// $ rm -rf
	err = os.RemoveAll("astaxie")
	if err != nil {
		fmt.Println(err)
	}

	// $ ls .
	normalScan()

	// $ tree .
	treeScan()
}

func normalScan() {
	p := "./sample"
	fileInfo, err := ioutil.ReadDir(p)
	if err != nil {
		fmt.Println(err)
	}

	for _, file := range fileInfo {
		if file.Mode().IsRegular() {
			fmt.Printf("The file: %v\n", p+"/"+file.Name())
		}

		if file.Mode().IsDir() {
			fmt.Printf("The directory: %v\n", p+"/"+file.Name())
		}
	}
}

func treeScan() {
	p := "./sample"
	err := filepath.Walk(p, func(path string,
		f os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		basepath, err := filepath.Rel(p, path)
		if err != nil {
			return err
		}

		if f.Mode().IsRegular() {
			fmt.Printf("Recursive scan file: %v\n", p+"/"+basepath)
		}

		if f.Mode().IsDir() {
			fmt.Printf("Recursive scan dir : %v\n", p+"/"+basepath)
		}

		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}
