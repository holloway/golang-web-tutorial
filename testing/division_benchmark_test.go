package gotest

import (
	"testing"
)

func Benchmark_Division(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Division(4, 5)
	}
}

func Benchmark_DivisionBySubtraction(b *testing.B) {
	for i := 0; i < b.N; i++ {
		DivisionBySubtraction(4, 5)
	}
}

func Benchmark_TimeConsumingFunction(b *testing.B) {
	b.StopTimer()

	// do some initialization pre-work here.

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		Division(4, 5)
	}
}
