package gotest

import (
	"testing"
	"errors"
)

func TestDivisionDrivenTable(t *testing.T) {
	// setup
	scenarios := []struct {
		in float64
		in2 float64
		out float64
		err error
	}{
		{6, 2, 3, nil},
		{6, 0, -2142, errors.New("")},
	}

	// test
	for _, scenario := range scenarios {
		i, e := Division(scenario.in, scenario.in2)
		if (scenario.err == nil && e != nil) ||
		   (scenario.err == nil && i != scenario.out) {
			t.Errorf("division failed: %v, %v, %v, %v\n",
				scenario.in,
				scenario.in2,
				scenario.out,
				scenario.err)
		}
	}
	// restore
}

func TestDivision1(t *testing.T) {
	if i, e := Division(6, 2); i == 3 && e == nil {
		// pass the test
		t.Log("division is working 6/2 = 3,0 with no error.")
		return
	}
	t.Error("division is not working 6/2.")
}

func TestDivision2(t *testing.T) {
	if _, e := Division(6, 0); e != nil {
		t.Log("division report errors when divisor is 0.", e)
		return
	}
	t.Error("division is not working when given 0 as divisor")
}

func TestDivisionBySubtraction_1(t *testing.T) {
	q, r, e := DivisionBySubtraction(6, 2)
	if q == 3 && r == 0 && e == nil {
		t.Log("division is working 6/2 = 3,0 with no error.")
		return
	}
	t.Error("division is not working 6/2.", q, r, e)
}

func TestDivisionBySubtraction_2(t *testing.T) {
	if _, _, e := DivisionBySubtraction(6, 0); e != nil {
		t.Log("division report errors when divisor is 0.", e)
		return
	}
	t.Error("division is not working when given 0 as divisor")
}
