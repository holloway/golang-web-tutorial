package gotest

import "errors"

// Division is a mathematical divide between a and b. Note that b cannot be 0
// since the it doesn't makes any sense to divide thing by void.
func Division(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("Divisor cannot be 0")
	}
	return a / b, nil
}

// DivisionBySubtraction is a mathematical divide using subtraction of a for
// b times. In another word, (a -a) repeat by b times.
func DivisionBySubtraction(a, b float64) (float64, float64, error) {
	if b == 0 {
		return 0, 0, errors.New("Divisor cannot be 0")
	}

	quotient := 0.00
	for {
		if a >= b {
			quotient++
			a -= b
			continue
		}
		break
	}
	return quotient, a, nil
}
