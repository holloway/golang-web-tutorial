package main

import (
	"encoding/base64"
	"crypto/rand"
	"crypto/subtle"
	"fmt"
	"golang.org/x/crypto/scrypt"
)

const (
	paraphrase = "Hello World"
)

func randomSalt(password string) ([]byte, error) {
	salt := make([]byte, 32)
	_, err := rand.Read(salt)
	if err != nil {
		return nil, err
	}
	hash, err := scrypt.Key([]byte(password), salt, 32768, 8, 1, 32)

	if err != nil {
		return nil, err
	}
	return hash, nil
}

func main() {
	// generate hashed password
	hash, err := randomSalt(paraphrase)
	if err != nil {
		fmt.Printf("error generating salt: %v\n", hash)
		return
	}

	// encode to base64 for database saving
	encode := base64.StdEncoding.EncodeToString(hash)
	fmt.Println(encode)

	// decode from base64 to original hashing
	decode, err := base64.StdEncoding.DecodeString(encode)
	if err != nil {
		fmt.Printf("error decoding hash: %v\n", err)
		return
	}

	// compare using subtle
	compare := subtle.ConstantTimeCompare(decode, hash) // 1 = true
	fmt.Println(compare)

	// random salt
	hash2, err := randomSalt(paraphrase)
	if err != nil {
		fmt.Printf("error generating hash2: %v\n", hash2)
		return
	}

	// compare original vs random
	compare = subtle.ConstantTimeCompare(hash, hash2)
	fmt.Println(compare)
	fmt.Println("It failed. Hence, keep the salt next to hash as a pair")
}
