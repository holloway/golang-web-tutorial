package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
)

func pad(src []byte) []byte {
	padding := aes.BlockSize - len(src)%aes.BlockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, padtext...)
}

func unpad(src []byte) ([]byte, error) {
	length := len(src)
	unpadding := int(src[length-1])

	if unpadding > length {
		return nil, errors.New("unpad error. This could happen when " +
			"incorrect encryption key is used")
	}

	return src[:(length - unpadding)], nil
}

// Encrypt encrypt a message with a given key. Ensure the
// provided key is byte based.
func Encrypt(key []byte, text string) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}

	message := gcm.Seal(nonce, nonce, []byte(text), nil)
	return base64.StdEncoding.EncodeToString(message), nil
}

// Decrypt reveals the message from an encrypted message using the
// given key.
func Decrypt(key []byte, text string) (string, error) {
	encryptedText, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	nonceSize := gcm.NonceSize()
	if len(encryptedText) < nonceSize {
		return "", errors.New("ciphertext too short")
	}

	nonce, encryptedText := encryptedText[:nonceSize],
		encryptedText[nonceSize:]
	message, err := gcm.Open(nil, nonce, encryptedText, nil)
	return string(message), err
}

// GenerateKey creates a random key for consumption. Remember
// to keep the key safe. Otherwise, you may lose your message!
func GenerateKey(length int) ([]byte, error) {
	key := make([]byte, length)
	_, err := rand.Read(key)
	if err != nil {
		return nil, err
	}
	return key, nil
}

func main() {
	key, _ := GenerateKey(32)
	encryptedMessage, _ := Encrypt(key, "Hello World")
	fmt.Println(encryptedMessage)

	message, _ := Decrypt(key, encryptedMessage)
	fmt.Println(message)
}
