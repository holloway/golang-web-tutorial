package main

import (
	"fmt"
	"time"
)

func fibonacci(c, quit chan int) {
	x, y := 1, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			close(c)
			fmt.Println("quit")
			return
		case <-time.After(5 * time.Second):
			// do timeout execution
		default:
			// do default execution if all channels are not ready
		}
	}
}

func main() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
		close(quit)
	}()
	fibonacci(c, quit)
}
