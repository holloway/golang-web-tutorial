package main

import (
	"fmt"
	"net/http"
	"time"
)

func indexHandler(w http.ResponseWriter, req *http.Request) {
	_, err := fmt.Fprint(w, "Hello. Start: /set, /update, /delete, /show")
	if err != nil {
		return
	}
}

func setCookie(w http.ResponseWriter, req *http.Request) {
	expire := time.Now().Add(365 * 24 * time.Hour)
	cookie := http.Cookie{Name: "username",
		Value: "astaxie", Expires: expire}
	http.SetCookie(w, &cookie)
	_, err := fmt.Fprint(w, "value set")
	if err != nil {
		return
	}
}

func updateCookie(w http.ResponseWriter, req *http.Request) {
	cookie, err := req.Cookie("username")
	if err != nil {
		expire := time.Now().Add(365 * 24 * time.Hour)
		cookie = &http.Cookie{Name: "username",
			Value: "Hacked!", Expires: expire}
	} else {
		cookie.Value = "Hacked!"
	}
	http.SetCookie(w, cookie)
	_, err = fmt.Fprint(w, "value set")
	if err != nil {
		return
	}
}

func showCookie(w http.ResponseWriter, req *http.Request) {
	value, err := req.Cookie("username")
	if err != nil {
		_, err = fmt.Fprint(w, "Oops. No cookie found")
		if err != nil {
			return
		}
		return
	}
	_, err = fmt.Fprint(w, value)
	if err != nil {
		return
	}
}

func deleteCookie(w http.ResponseWriter, req *http.Request) {
	cookie, err := req.Cookie("username")
	if err != nil {
		_, err = fmt.Fprint(w, "No cookie found.")
		if err != nil {
			return
		}
	}
	cookie.MaxAge = -1
	http.SetCookie(w, cookie)
	_, err = fmt.Fprint(w, "Cookie deleted!")
	if err != nil {
		return
	}
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/set", setCookie)
	http.HandleFunc("/update", updateCookie)
	http.HandleFunc("/delete", deleteCookie)
	http.HandleFunc("/show", showCookie)
	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		return
	}
}
